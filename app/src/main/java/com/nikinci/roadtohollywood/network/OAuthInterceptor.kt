package com.nikinci.roadtohollywood.network

import com.nikinci.roadtohollywood.core.GDriveManager
import okhttp3.Interceptor
import javax.inject.Inject


class OAuthInterceptor @Inject constructor(var driverManager: GDriveManager) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder().header(
            "Authorization", "Bearer ${driverManager.credential.accessToken}"
        ).header(
            "OriginalUrl", "${request.url}"
        ).build()

        return chain.proceed(request)
    }


}