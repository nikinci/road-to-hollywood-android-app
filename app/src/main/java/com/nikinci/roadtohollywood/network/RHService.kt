package com.nikinci.roadtohollywood.network

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Streaming
import retrofit2.http.Url

/**
 * Created by nikinci on 2019-02-27.
 */
interface RHService {
    companion object {

    }

    @Streaming
    @GET
    @Headers(
        "Accept: application/vnd.oasis.opendocument.spreadsheet"
    )
    fun getFile(
        @Url fileUrl: String
    ): Call<ResponseBody>

    @Streaming

    @GET
    @Headers(
        "Accept: application/vnd.oasis.opendocument.spreadsheet"
    )
    fun getFileObservable(
        @Url fileUrl: String
    ): Observable<Response<ResponseBody>>
}