package com.nikinci.roadtohollywood.network

import com.nikinci.roadtohollywood.core.GDriveManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class RedirectInterceptor @Inject constructor(var driverManager: GDriveManager) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request: Request = chain.request()
        var response: Response = chain.proceed(chain.request())

        try {
            while (response.code == 307) {
                request = request.newBuilder()
                    .url(response.header("location")!!)
                    .build()
                response = chain.proceed(request)
            }
        } catch (e: Exception) {
            e.message
        }
        return response
    }
}

