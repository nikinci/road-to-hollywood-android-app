package com.nikinci.roadtohollywood.ui.detailed

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nikinci.roadtohollywood.core.GDriveManager
import com.nikinci.roadtohollywood.data.repository.GoogleDriveRepository
import com.nikinci.roadtohollywood.model.RHRevision
import com.nikinci.roadtohollywood.model.RevisionDiff
import com.nikinci.roadtohollywood.util.ExcelDiffer
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class DetailActViewModel @Inject constructor(
    private val driveManager: GDriveManager,
    private val googleDriveRepository: GoogleDriveRepository,
    @ApplicationContext private val appContext: Context
) :
    ViewModel() {
    val loading = MutableLiveData<Boolean>()
    private val _revisionDiffList =
        MutableLiveData<List<RevisionDiff>>()
    val revisionDiffListLiveData: LiveData<List<RevisionDiff>> =
        _revisionDiffList


    fun downloadAndFillDiff(currRev: RHRevision, prevRev: RHRevision?) {
        loading.value = true
        val revisionList = mutableListOf<RHRevision>().apply {
            add(currRev)
            prevRev?.let {
                add(it)
            }
        }
        val pairList =
            revisionList.filter { it.exportLink.map { it.endsWith("xlsx") }.isNotEmpty() }.map {
                Pair(
                    (it.exportLink.first() { it.endsWith("xlsx") }),
                    it.getFileName()
                )
            }.toMutableList()

        val notExistingFilesPair = pairList.filter {
            java.io.File(appContext.filesDir, "${it.second}.xlsx").exists().not()
        }

        googleDriveRepository.downloadFileList(notExistingFilesPair, {
            viewModelScope.launch(Dispatchers.IO) {
                val diffList = fillRevDiffFromFile(currRev, prevRev)
                _revisionDiffList.postValue(diffList)
                loading.postValue(false)
            }

        }, {
            //TODO handle error case
            loading.postValue(false)
        })
    }


    // it calculates diff between two revision
    private fun fillRevDiffFromFile(
        rev: RHRevision,
        prevRev: RHRevision?
    ): MutableList<RevisionDiff> {
        val diffList: MutableList<RevisionDiff> = if (prevRev != null) {
            ExcelDiffer.diff2ExcelFile(
                appContext,
                rev,
                prevRev
            )
        } else {
            mutableListOf()
        }

        return diffList

    }

}