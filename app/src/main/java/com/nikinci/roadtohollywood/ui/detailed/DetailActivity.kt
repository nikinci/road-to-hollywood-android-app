package com.nikinci.roadtohollywood.ui.detailed

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.nikinci.roadtohollywood.R
import com.nikinci.roadtohollywood.base.BaseBindingActivity
import com.nikinci.roadtohollywood.data.repository.GoogleDriveRepository
import com.nikinci.roadtohollywood.databinding.ActivityDetailBinding
import com.nikinci.roadtohollywood.databinding.ItemRevisionDiffBinding
import com.nikinci.roadtohollywood.extension.observe
import com.nikinci.roadtohollywood.extension.setUpBindingAdapter
import com.nikinci.roadtohollywood.model.RHRevision
import com.nikinci.roadtohollywood.model.RevisionDiff
import com.nikinci.roadtohollywood.model.RevisionDiffGroup
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class DetailActivity : BaseBindingActivity<ActivityDetailBinding>() {


    val vm: DetailActViewModel by viewModels()

    @Inject
    lateinit var googleDriveRepository: GoogleDriveRepository

    private val currRevision: RHRevision by lazy {
        intent.getSerializableExtra(
            BUNDLE_KEY_CURRENT_REV
        ) as RHRevision
    }

    private val prevRevision: RHRevision? by lazy {
        intent.getSerializableExtra(
            BUNDLE_KEY_PREV_REV
        ) as RHRevision
    }

    companion object {
        const val BUNDLE_KEY_CURRENT_REV = "bundle.rev.curr"
        const val BUNDLE_KEY_PREV_REV = "bundle.rev.prev"
        fun newIntent(context: Context, curr: RHRevision, prev: RHRevision?): Intent {
            return Intent(context, DetailActivity::class.java).apply {
                putExtra(BUNDLE_KEY_CURRENT_REV, curr)
                putExtra(BUNDLE_KEY_PREV_REV, prev)
            }
        }
    }


    override fun getContentViewLayoutResId() = R.layout.activity_detail

    override fun populateUI(savedInstanceState: Bundle?) {
        //ex: file url https://docs.google.com/spreadsheets/export?id=1VWuF4w6DZXcsmGrhKn9LVvygw2lNSX1JpVsfERntmWc&revision=1&exportFormat=xlsx
        mBinding?.vm = vm
        mBinding?.currRev = currRevision

        //action bar title and click settings
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = "Rev Id: ${currRevision.id}"
        }

        vm.downloadAndFillDiff(currRevision, prevRevision)
        setObservers()
    }

    private fun setObservers() {
        observe(vm.revisionDiffListLiveData) {
            it?.size

            it?.let { revList ->
                setList(getDiffGroupList(revList))

            }
        }

    }

    private fun setList(groupList: List<RevisionDiffGroup>) {
        mBinding?.recyclerview?.setUpBindingAdapter(
            groupList,
            R.layout.item_revision_diff,
            bindHolder = { holder, revisionDiffGroup, i ->

                (holder.binding as ItemRevisionDiffBinding).apply {
                    this.recyclerDiffChange.setUpBindingAdapter(
                        revisionDiffGroup.diffList,
                        R.layout.item_revision_change
                    )
                }
            })
    }

    private fun getDiffGroupList(diffList: List<RevisionDiff>): List<RevisionDiffGroup> {

        val tempMap = hashMapOf<String, RevisionDiffGroup>()
        diffList.forEach {
            if (tempMap[it.rowName] == null) {
                tempMap[it.rowName] = RevisionDiffGroup()
            }
            tempMap[it.rowName]?.diffList?.add(it)
        }
        return tempMap.map {
            RevisionDiffGroup().apply {
                groupName = it.key
                this.diffList.addAll(it.value.diffList)
            }
        }

    }
}