package com.nikinci.roadtohollywood.ui.home.summary

import android.view.View
import androidx.fragment.app.viewModels
import com.nikinci.roadtohollywood.R
import com.nikinci.roadtohollywood.base.BaseBindingFragment
import com.nikinci.roadtohollywood.core.GDriveManager
import com.nikinci.roadtohollywood.databinding.FragmentRevisionsSummaryBinding
import com.nikinci.roadtohollywood.databinding.ItemSummaryRevisionBinding
import com.nikinci.roadtohollywood.extension.observe
import com.nikinci.roadtohollywood.extension.setUpBindingAdapter
import com.nikinci.roadtohollywood.extension.toast
import com.nikinci.roadtohollywood.model.RHRevision
import com.nikinci.roadtohollywood.ui.detailed.DetailActivity
import com.nikinci.roadtohollywood.util.AbstractRecyclerBindingAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class RevisionSummaryFragment : BaseBindingFragment<FragmentRevisionsSummaryBinding>() {


    val vm: RevisionSummaryViewModel by viewModels()

    @Inject
    lateinit var driveManager: GDriveManager

    override fun populateUI(rootView: View) {
        mBinding?.vm = this.vm
        mBinding?.swipeRefresh?.apply {
            setOnRefreshListener {
                vm.getRevisionList(vm.FILE_NAME)
            }
        }
        setObservers()
    }

    private fun setObservers() {
        observe(vm.loading) {
            if (true != it) {
                mBinding?.swipeRefresh?.isRefreshing = it ?: false
            }
        }

        //it observes revision list and show in view
        observe(vm.revisionListLiveData) {
            it?.let {
                mBinding?.recyclerview?.setUpBindingAdapter(
                    it,
                    R.layout.item_summary_revision,
                    bindHolder = { holder: AbstractRecyclerBindingAdapter.HolderBinding, rhRevision: RHRevision, i: Int ->
                        (holder.binding as ItemSummaryRevisionBinding).apply {
                            layRoot.setOnClickListener {
                                var prevRev: RHRevision? = null
                                if ((i + 1) < vm.revisionListLiveData.value?.size ?: 0) {
                                    prevRev = vm.revisionListLiveData.value!![i + 1]
                                }
                                //handles item click if it has not prev revision it shows warning msg
                                if (prevRev != null) {
                                    startActivity(
                                        DetailActivity.newIntent(
                                            requireContext(),
                                            rhRevision, prevRev
                                        )
                                    )
                                } else {
                                    context?.toast(getString(R.string.summary_warning_not_have_prev_rev))
                                }
                            }
                        }
                    })
            }

        }

    }

    override fun getContentLayoutResId() = R.layout.fragment_revisions_summary


}


