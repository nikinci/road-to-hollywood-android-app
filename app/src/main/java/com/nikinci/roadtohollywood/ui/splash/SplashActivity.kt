package com.nikinci.roadtohollywood.ui.splash

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.nikinci.roadtohollywood.MainActivity
import com.nikinci.roadtohollywood.R
import com.nikinci.roadtohollywood.base.BaseBindingActivity
import com.nikinci.roadtohollywood.databinding.ActivitySplashBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@AndroidEntryPoint
class SplashActivity : BaseBindingActivity<ActivitySplashBinding>() {


    override fun getContentViewLayoutResId() = R.layout.activity_splash

    override fun populateUI(savedInstanceState: Bundle?) {
        lifecycleScope.launch {
            delay(2000)
            startActivity(MainActivity.newIntent(this@SplashActivity))
            finish()
        }
    }
}