package com.nikinci.roadtohollywood.ui.home.summary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.api.services.drive.model.File
import com.nikinci.roadtohollywood.core.GDriveManager
import com.nikinci.roadtohollywood.extension.toRHRevision
import com.nikinci.roadtohollywood.model.RHRevision
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class RevisionSummaryViewModel @Inject constructor(private val driveManager: GDriveManager) :
    ViewModel() {
    val loading = MutableLiveData<Boolean>()
    private val _revisionList =
        MutableLiveData<List<RHRevision>>()
    val revisionListLiveData: LiveData<List<RHRevision>> =
        _revisionList

    val FILE_NAME = "Road to Hollywood"

    init {
        getRevisionList(FILE_NAME)
    }

    fun getRevisionList(fileName: String) {
        loading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val revisions = accessDriveFileForRevisions(fileName)
            _revisionList.postValue(revisions)
            loading.postValue(false)
        }
    }


    //accessing file with filename
    private fun accessDriveFileForRevisions(fileName: String): List<RHRevision> {
        driveManager.getDriveService()?.let { googleDriveService ->
            var pageToken: String? = null
            do {
                val result = googleDriveService.files().list().apply {
                    spaces = "drive"
                    fields = "nextPageToken, files(id, name)"
                    pageToken = this.pageToken
                }.execute()

                for (file in result.files) {
                    if (file.name.equals(fileName, true)) {
                        return getRevisions(file)
                    }
                }
            } while (pageToken != null)

        }
        return emptyList()
    }

    //getting revisions for file
    private fun getRevisions(file: File): List<RHRevision> {
        val revisionList = mutableListOf<RHRevision>()
        driveManager.getDriveService()?.let { googleDriveService ->
            //maxResult default 200 nextPage not required now
            val result = googleDriveService.revisions().list(file.id).apply {
                fields = "revisions(id,lastModifyingUser,modifiedTime,exportLinks,published)"
            }.execute()

            result.revisions?.forEach { rev ->

                val element = rev.toRHRevision()
                element.let {
                    val values1 =
                        (rev["exportLinks"] as com.google.api.client.util.ArrayMap<String, String>).values.toList()
                    it.exportLink = values1
                }
                revisionList.add(element)

            }
        }
        return revisionList.reversed()
    }


}