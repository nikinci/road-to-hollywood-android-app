package com.nikinci.roadtohollywood.ui.detailed

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.api.services.drive.model.File
import com.nikinci.roadtohollywood.core.GDriveManager
import com.nikinci.roadtohollywood.data.repository.GoogleDriveRepository
import com.nikinci.roadtohollywood.extension.toRHRevision
import com.nikinci.roadtohollywood.model.RHRevision
import com.nikinci.roadtohollywood.network.RHService
import com.nikinci.roadtohollywood.util.ExcelDiffer
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.FileOutputStream
import java.io.OutputStream
import javax.inject.Inject


@HiltViewModel
@Deprecated("detail screen changed to Activity,DetaolFragment was using this class")
class RevisionDetailViewModel @Inject constructor(
    private val driveManager: GDriveManager,
    @ApplicationContext var appContext: Context,
    private val googleDriveRepository: GoogleDriveRepository,
    val service: RHService
) :
    ViewModel() {
    val loading = MutableLiveData<Boolean>()
    private val _revisionList =
        MutableLiveData<List<RHRevision>>()
    val revisionListLiveData: LiveData<List<RHRevision>> =
        _revisionList
    val FILE_NAME = "Road to Hollywood"
    var currentFileId: String? = null

    init {
        getRevisionList(FILE_NAME)
    }

    fun getRevisionList(fileName: String) {
        loading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val revisions = accessDriveFileForRevisions(fileName)
            fillDiff(revisions)
            _revisionList.postValue(revisions)

        }
    }

    fun fillDiff(revisionList: List<RHRevision>) {
        if (revisionList.size < 2) {
            return
        }

        val pairList =
            revisionList.filter { it.exportLink.map { it.endsWith("xlsx") }.isNotEmpty() }.map {
                Pair(
                    (it.exportLink.first() { it.endsWith("xlsx") }),
                    it.getFileName()
                )
            }.toMutableList()

        val notExistingFilesPair = pairList.filter {
            java.io.File(appContext.filesDir, "${it.second}.xlsx").exists().not()
        }

        googleDriveRepository.downloadFileList(notExistingFilesPair, {
            loading.postValue(false)

        }, {
            loading.postValue(false)
        })


    }


    suspend fun fillRevDiff(rev: RHRevision, prevRev: RHRevision) {
        val diff2ExcelFile = ExcelDiffer.diff2ExcelFile(
            appContext,
            rev,
            prevRev
        )
        diff2ExcelFile.size

    }

    private suspend fun accessDriveFileForRevisions(fileName: String): List<RHRevision> {
        driveManager.getDriveService()?.let { googleDriveService ->
            var pageToken: String? = null
            do {
                val result = googleDriveService.files().list().apply {
                    spaces = "drive"
                    fields = "nextPageToken, files(id, name)"
                    pageToken = this.pageToken
                }.execute()

                //driveManager.accessToken = pageToken
                for (file in result.files) {
                    val s = "name=${file.name} id=${file.id}"

                    if (file.name.equals(fileName, true)) {
                        //downloadAndCheckFile(file)

                        return getRevisions(file).sortedByDescending { it.id }
                    }
                }
            } while (pageToken != null)

        }
        return mutableListOf()
    }

    private suspend fun getRevisions(file: File): List<RHRevision> {
        currentFileId = file.id
        val revisionList = mutableListOf<RHRevision>()
        driveManager.getDriveService()?.let { googleDriveService ->
            //maxResult default 200 nextPage not required now
            val result = googleDriveService.revisions().list(file.id).apply {
                fields = "revisions(id,lastModifyingUser,modifiedTime,exportLinks)"
            }.execute()

            result.revisions?.forEach { rev ->

                val element = rev.toRHRevision()
                element.let {
                    val values1 =
                        (rev["exportLinks"] as com.google.api.client.util.ArrayMap<String, String>).values.toList()
                    it.exportLink = values1
                }
                revisionList.add(element)

/*
                val revisionRes = googleDriveService.revisions().get(file.id, it.id).apply {
                    fields = "id,lastModifyingUser,modifiedTime,exportLinks"
                }.execute()
                revisionRes.apply {
                    val element = this.toRHRevision()
                    element.let {
                        val values1 =
                            (this["exportLinks"] as com.google.api.client.util.ArrayMap<String, String>).values.toList()
                        it.exportLink = values1
                    }
                    revisionList.add(element)
                }*/
            }
        }
        return revisionList
    }


    private suspend fun downloadFileIfNeeded(file: File, fileName: String) {
        val driveService = driveManager.getDriveService()
        val gDriveFile = driveService?.Files()?.get(file.id)?.execute()

        val wFile = java.io.File(appContext.filesDir, "tempRoadFile.xlsx")

        val out: OutputStream =
            FileOutputStream(wFile)


        driveService?.Files()
            ?.export(file.id, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            ?.executeMediaAndDownloadTo(out);


    }


    private fun downloadFile(url: String, fileName: String) {
        val wFile = java.io.File(appContext.filesDir, "$fileName.xlsx")
        if (wFile.exists()) {
            return
        }
        googleDriveRepository.downloadAndSaveFile(url, fileName)
        return

    }


}