package com.nikinci.roadtohollywood.ui.home

import android.view.View
import androidx.navigation.fragment.NavHostFragment
import com.nikinci.roadtohollywood.R
import com.nikinci.roadtohollywood.base.BaseBindingFragment
import com.nikinci.roadtohollywood.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint


@Deprecated("detail screen changed to Activity,Summary fragment becomes new Home ")
@AndroidEntryPoint
class HomeFragment : BaseBindingFragment<FragmentHomeBinding>() {


    override fun populateUI(rootView: View) {
        mBinding?.apply {
            buttonDetailed.setOnClickListener {
                NavHostFragment.findNavController(this@HomeFragment)
                    .navigate(R.id.action_nav_home_to_nav_revisions_detail)

            }
            buttonSummary.setOnClickListener {
                NavHostFragment.findNavController(this@HomeFragment)
                    .navigate(R.id.action_nav_home_to_nav_revisions_summary)

            }
        }
    }

    override fun getContentLayoutResId() = R.layout.fragment_home


}


