package com.nikinci.roadtohollywood.ui.detailed

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.drive.model.ChangeList
import com.google.api.services.drive.model.File
import com.google.api.services.drive.model.StartPageToken
import com.nikinci.roadtohollywood.R
import com.nikinci.roadtohollywood.base.BaseBindingFragment
import com.nikinci.roadtohollywood.databinding.FragmentRevisionsDetailBinding
import com.nikinci.roadtohollywood.extension.observe
import com.nikinci.roadtohollywood.extension.setUpBindingAdapter
import com.nikinci.roadtohollywood.model.RHUser
import com.nikinci.roadtohollywood.model.UserActLog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.FileOutputStream
import java.io.OutputStream


@Deprecated("detail screen changed to Activity")
@AndroidEntryPoint
class RevisionsDetailFragment : BaseBindingFragment<FragmentRevisionsDetailBinding>() {

    companion object {
        fun newInstance() = RevisionsDetailFragment()
    }


    val vm: RevisionDetailViewModel by viewModels()

    val FILE_NAME = "Road to Hollywood"

    var mDriveService: Drive? = null


    lateinit var textView: TextView


    override fun populateUI(rootView: View) {
        mBinding?.vm = this.vm
        observe(vm.revisionListLiveData) {
            it?.let {
                lifecycleScope.launch(Dispatchers.Default) {
                    vm.fillRevDiff(it[0], it[1])
                }
                mBinding?.recyclerview?.setUpBindingAdapter(it, R.layout.item_summary_revision)
            }

        }
        observe(vm.loading) {
            //  setLoading(it)
            mBinding?.swipeRefresh?.isRefreshing = it ?: false
        }

        mBinding?.swipeRefresh?.apply {
            setOnRefreshListener {
                vm.getRevisionList(vm.FILE_NAME)
            }
        }
        lifecycleScope.launch(Dispatchers.IO) {

            accessDriveFileForRevisions(FILE_NAME)
            // accessDriveFileForChanges(FILE_NAME)
            /* val accessDriveFiles = accessDriveFile(FILE_NAME)
             accessDriveFiles.forEach {
                 it
             }*/
        }

    }

    override fun getContentLayoutResId() = R.layout.fragment_revisions_detail


    private fun getDriveService(): Drive? {
        /*   val credential = GoogleAccountCredential.usingOAuth2(
               requireContext(), listOf(DriveScopes.DRIVE_FILE)
           )*/
        mDriveService?.let {
            return it
        }
        val credential =
            GoogleCredential.fromStream(resources.openRawResource(R.raw.road_to_hollywood_e2f4102922e0))
                .createScoped(
                    listOf(
                        DriveScopes.DRIVE
                    )
                )


        mDriveService = Drive.Builder(
            NetHttpTransport(),
            JacksonFactory.getDefaultInstance(),
            credential
        )
            .setApplicationName("road-to-hollywood")
            .build()
        return mDriveService
    }


    suspend fun getRevisions(file: File) {
        getDriveService()?.let { googleDriveService ->
            //maxResult default 200 nextPage not required now
            val result = googleDriveService.revisions().list(file.id).apply {

            }.execute()

            result.revisions?.forEach {
                it.id

                val revisionRes = googleDriveService.revisions().get(file.id, it.id).apply {
                    fields = "id,lastModifyingUser,modifiedTime,exportLinks"
                }.execute()
                revisionRes.apply {
                    this.lastModifyingUser
                }
            }
        }
    }


    suspend fun accessDriveFileForRevisions(fileName: String) {
        getDriveService()?.let { googleDriveService ->
            var pageToken: String? = null
            do {
                val result = googleDriveService.files().list().apply {
                    spaces = "drive"
                    fields = "nextPageToken, files(id, name)"
                    pageToken = this.pageToken
                }.execute()

                for (file in result.files) {
                    val s = "name=${file.name} id=${file.id}"

                    if (file.name.equals(fileName, true)) {
                        //downloadAndCheckFile(file)
                        getRevisions(file)
                        return
                    }
                }
            } while (pageToken != null)

        }

    }

    suspend fun accessDriveFileForChanges(fileName: String): List<String> {
        val fileList = mutableListOf<String>()
        getDriveService()?.let { googleDriveService ->


            val response: StartPageToken = googleDriveService.changes()
                .startPageToken.execute()
            var startPageToken = response.startPageToken
            var pageToken = startPageToken
            while (pageToken != null) {
                val changes: ChangeList =
                    googleDriveService.changes().list(pageToken).apply {
                        isIncludeTeamDriveItems = true
                        isSupportsTeamDrives = true


                    }.execute()

                for (change in changes.changes) {
                    // Process change
                    println("Change found for file: " + change.fileId)
                }
                if (changes.newStartPageToken != null) {
                    // Last page, save this token for the next polling interval
                    startPageToken = changes.newStartPageToken
                }
                pageToken = changes.nextPageToken
            }


        }
        return fileList
    }


    suspend fun accessDriveFile(fileName: String): List<String> {
        val fileList = mutableListOf<String>()
        getDriveService()?.let { googleDriveService ->
            var pageToken: String? = null
            do {
                val result = googleDriveService.files().list().apply {
                    spaces = "drive"
                    fields = "nextPageToken, files(id, name)"
                    pageToken = this.pageToken
                }.execute()

                for (file in result.files) {
                    val s = "name=${file.name} id=${file.id}"

                    if (file.name.equals(fileName, true)) {
                        downloadAndCheckFile(file)
                    }
                    fileList.add(s)
                }
            } while (pageToken != null)

        }
        return fileList
    }

    private suspend fun downloadAndCheckFile(file: File) {
        val gDriveFile = getDriveService()?.Files()?.get(file.id)?.execute()

        val wFile = java.io.File(requireContext().filesDir, "tempRoadFile.xlsx")

        val out: OutputStream =
            FileOutputStream(wFile)


        getDriveService()?.Files()
            ?.export(file.id, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            ?.executeMediaAndDownloadTo(out);
        parseExcelFile(wFile)

    }


    private fun parseExcelFile(file: java.io.File): List<UserActLog> {

        val rTHWorkBook = XSSFWorkbook(file)
        val sheet: XSSFSheet = rTHWorkBook.getSheetAt(0)
        val rowIter: Iterator<Row> = sheet.rowIterator()
        var rowNumber = 0
        var userNameFinished = false
        val userActLogList = mutableListOf<UserActLog>()

        while (rowIter.hasNext() && userNameFinished.not()) {
            val myRow = rowIter.next() as XSSFRow
            val cellIter: Iterator<Cell> = myRow.cellIterator()
            var colno = 0


            var userName = ""
            var userTotalMiles: Double = 0.0
            val userLogList = mutableListOf<Double>()
            if (rowNumber >= 5) {
                while (cellIter.hasNext() && userNameFinished.not()) {
                    val currentCell = cellIter.next()
                    if (colno == 0) {
                        userName = currentCell.toString()

                        if (userName.isEmpty()) {
                            userNameFinished = true
                        }
                    } else if (colno == 1) {
                        userTotalMiles = currentCell.numericCellValue
                    } else {
                        if (currentCell.toString().isNotEmpty()) {
                            userLogList.add(currentCell.numericCellValue)
                        }

                    }
                    colno++

                }

                if (userNameFinished.not()) {
                    userActLogList.add(UserActLog().apply {
                        RHUser = RHUser(userName)
                        totalActMiles = userTotalMiles
                        activityLogs = userLogList
                    })

                    Log.e(

                        "Excell",
                        " $userName : Total: $userTotalMiles + LogCount = ${userLogList.size}"
                    )
                }
            }
            rowNumber++
        }
        return userActLogList

    }

}


