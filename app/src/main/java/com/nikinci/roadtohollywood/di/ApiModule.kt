package com.nikinci.roadtohollywood.di

import com.nikinci.roadtohollywood.network.RHService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by nikinci on 11/16/21.
 */
@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"

    @Provides
    @Singleton
    fun provideHotDealsService(retrofit: Retrofit): RHService {
        return retrofit.create(RHService::class.java)
    }
}

