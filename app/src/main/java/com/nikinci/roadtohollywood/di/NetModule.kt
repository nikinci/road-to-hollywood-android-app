package com.nikinci.roadtohollywood.di

import android.content.Context
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.nikinci.roadtohollywood.network.OAuthInterceptor
import com.nikinci.roadtohollywood.network.RedirectInterceptor
import com.nikinci.roadtohollywood.util.UrlConstants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier


/**
 * Created by nikinci on 2019-02-27.
 */
@Module
@InstallIn(SingletonComponent::class)
class NetModule {
    companion object {
        const val BASE_URL = "BASE_URL"
        const val SSL_CERTIFICATE = "SSL_CERTIFICATE"
        const val CONNECT_TIMEOUT_IN_SECONDS_PROD: Long = 90
        const val READ_TIMEOUT_IN_SECONDS_PROD: Long = 90
        const val WRITE_TIMEOUT_IN_SECONDS_PROD: Long = 90
    }

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class BaseUrl

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class SSLCertificate

    @Provides
    @BaseUrl
    fun provideBaseUrl(): String {
        return UrlConstants.BASE_URL
    }

    @Provides
    @SSLCertificate
    fun provideSSLCertificate(): String {
        return "NOT_ENABLED-NOT_USING"
//sha256/X1N5AGYSN1y5sS/ZWgDR1ft8qCuv9er4mBtt6pKGiak=
    }

    @Provides
    fun provideOkHttpCache(@ApplicationContext context: Context): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(context.cacheDir, cacheSize.toLong())
    }

    @Provides
    fun provideCookieJar(): CookieJar {
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        return JavaNetCookieJar(cookieManager)
    }

    @Provides
    fun provideCertificatePinner(@SSLCertificate certificate: String): CertificatePinner {
        return CertificatePinner.Builder()
            /*  .add(
                  UrlConstants.BASE_URL.removeUrlProtocol(),
                  certificate
              )*/
            .build()
    }

    @Provides
    fun provideObjectMapper(): ObjectMapper {
        return ObjectMapper().apply {
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true)
        }
    }

    @Provides
    fun provideJacksonConverterFactory(objectMapper: ObjectMapper): JacksonConverterFactory {
        return JacksonConverterFactory.create(objectMapper)
    }

    @Provides
    fun provideOkHttpClient(
        cache: Cache,
        cookieJar: CookieJar,
        @ApplicationContext context: Context,
        oAuthInterceptor: OAuthInterceptor,
        redirectInterceptor: RedirectInterceptor,
    ): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.cookieJar(cookieJar)
        clientBuilder.addInterceptor(oAuthInterceptor)
        //clientBuilder.addInterceptor(redirectInterceptor)
        /*clientBuilder.followRedirects(true)
        clientBuilder.followSslRedirects(true)*/
        clientBuilder.connectTimeout(CONNECT_TIMEOUT_IN_SECONDS_PROD, TimeUnit.SECONDS)
        clientBuilder.readTimeout(READ_TIMEOUT_IN_SECONDS_PROD, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(WRITE_TIMEOUT_IN_SECONDS_PROD, TimeUnit.SECONDS)


        return clientBuilder.build()
    }

    @Provides
    fun provideRetrofit(
        converterFactory: JacksonConverterFactory,
        okHttpClient: OkHttpClient, @BaseUrl
        mBaseUrl: String
    ): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(converterFactory)
            .baseUrl(mBaseUrl)
            .client(okHttpClient)
            .build()
    }
}