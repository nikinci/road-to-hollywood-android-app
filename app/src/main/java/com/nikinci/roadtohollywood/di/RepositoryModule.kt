package com.nikinci.roadtohollywood.di

import android.content.Context
import com.nikinci.roadtohollywood.data.repository.GoogleDriveRepository
import com.nikinci.roadtohollywood.data.source.GoogleDriveDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient

/**
 * Created by nikinci on 11/16/21.
 */
@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {


    @Provides
    fun provideGoogleDriveRepository(
        dataSource: GoogleDriveDataSource,
        @ApplicationContext context: Context,
        okHttpClient: OkHttpClient
    ): GoogleDriveRepository =
        GoogleDriveRepository(dataSource, context, okHttpClient)

}
