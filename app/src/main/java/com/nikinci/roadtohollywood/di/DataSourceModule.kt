package com.nikinci.roadtohollywood.di

import com.nikinci.roadtohollywood.data.remote.GoogleDriveRemoteDataSource
import com.nikinci.roadtohollywood.data.source.GoogleDriveDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by nikinci on 11/16/21.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    abstract fun bindGoogleDriveDataSource(
        dataSource: GoogleDriveRemoteDataSource
    ): GoogleDriveDataSource


}
