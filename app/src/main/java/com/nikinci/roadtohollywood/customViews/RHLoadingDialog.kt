package com.nikinci.roadtohollywood.customViews

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialog
import com.nikinci.roadtohollywood.R

class RHLoadingDialog(context: Context) : AppCompatDialog(context, R.style.Theme_App_Dialog) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_dialog_loading)

        setCancelable(false)
        setCanceledOnTouchOutside(false)

    }


}