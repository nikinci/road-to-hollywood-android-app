package com.nikinci.roadtohollywood.model

import java.io.Serializable
import java.util.*

data class RHRevision(
    var id: String,
    var user: RHUser,
    var published: Boolean = false,
    var modifieedTime: Date? = null,
    var exportLink: List<String> = mutableListOf(),
    var diffList: MutableList<RevisionDiff> = mutableListOf()
) : Serializable {
    fun getFileName(): String {
        return "roadToHollyWood_rev_$id"
    }
}
