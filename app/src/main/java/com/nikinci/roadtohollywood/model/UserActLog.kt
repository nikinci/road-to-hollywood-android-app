package com.nikinci.roadtohollywood.model

class UserActLog {
    var RHUser: RHUser? = null
    var activityLogs = mutableListOf<Double>()
    var totalActMiles: Double = 0.0
}