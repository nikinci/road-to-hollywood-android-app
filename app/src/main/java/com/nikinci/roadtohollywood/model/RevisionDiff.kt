package com.nikinci.roadtohollywood.model

class RevisionDiff {
    var rowNo: Int = 0
    var colNo: Int = 0
    var rowName: String = ""
    var colName: String = ""
    var currentValue: String = ""
    var oldValue: String = ""
}