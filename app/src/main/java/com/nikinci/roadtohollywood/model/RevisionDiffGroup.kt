package com.nikinci.roadtohollywood.model

class RevisionDiffGroup {
    var groupName: String = ""
    var diffList = mutableListOf<RevisionDiff>()
}