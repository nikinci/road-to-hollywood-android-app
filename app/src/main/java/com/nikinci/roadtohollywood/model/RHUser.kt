package com.nikinci.roadtohollywood.model

import java.io.Serializable

data class RHUser(
    var displayName: String? = null,
    var emailAddress: String? = null,
    var photoLink: String? = null
) : Serializable

