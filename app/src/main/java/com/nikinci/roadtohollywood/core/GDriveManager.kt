package com.nikinci.roadtohollywood.core

import android.content.Context
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.nikinci.roadtohollywood.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GDriveManager @Inject constructor(@ApplicationContext var appContext: Context) {
    private var mDriveService: Drive? = null
    lateinit var credential: GoogleCredential

    fun getDriveService(): Drive? {
        mDriveService?.let {
            return it
        }

        credential =
            GoogleCredential.fromStream(appContext.resources.openRawResource(R.raw.road_to_hollywood_e2f4102922e0))
                .createScoped(
                    listOf(
                        DriveScopes.DRIVE
                    )
                )



        mDriveService = Drive.Builder(
            NetHttpTransport(),
            JacksonFactory.getDefaultInstance(),
            credential
        )
            .setApplicationName("road-to-hollywood")
            .build()
        return mDriveService
    }


}