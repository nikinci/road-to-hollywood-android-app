package com.nikinci.roadtohollywood.core

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by nikinci on 11/16/21.
 */
@HiltAndroidApp
class RHApp : MultiDexApplication() {


    companion object {
        lateinit var instance: RHApp
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}