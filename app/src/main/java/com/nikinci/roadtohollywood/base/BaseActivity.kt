package com.nikinci.roadtohollywood.base

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.hilt.android.AndroidEntryPoint

abstract class BaseBindingActivity<T : ViewDataBinding> : BaseActivity() {
    protected var mBinding: T? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, getContentViewLayoutResId())
        mBinding?.lifecycleOwner = this
        populateUI(savedInstanceState)
    }
}

@AndroidEntryPoint
abstract class BaseActivity : AppCompatActivity() {
    companion object {
    }

    @LayoutRes
    abstract fun getContentViewLayoutResId(): Int
    protected var mDialog: Dialog? = null


    protected abstract fun populateUI(savedInstanceState: Bundle?)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}