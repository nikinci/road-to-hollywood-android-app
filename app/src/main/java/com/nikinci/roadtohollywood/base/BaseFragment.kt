package com.nikinci.roadtohollywood.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.nikinci.roadtohollywood.extension.showLoadingDialog
import dagger.hilt.android.AndroidEntryPoint

abstract class BaseBindingFragment<T : ViewDataBinding> : BaseFragment() {

    protected var mBinding: T? = null
    protected var mDialog: Dialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(
            inflater, getContentLayoutResId(), container, false
        )
        mBinding?.lifecycleOwner = viewLifecycleOwner
        return mBinding?.root
    }
}

@AndroidEntryPoint
abstract class BaseFragment : Fragment() {
    protected abstract fun populateUI(rootView: View)


    open fun getFragmentTag(): String {
        return javaClass.simpleName
    }

    protected var loadingDialog: Dialog? = null

    companion object {
        val BUNDLE_KEY_ITEM = "bundle.key.item"
    }

    @LayoutRes
    protected abstract fun getContentLayoutResId(): Int


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getContentLayoutResId(), container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateUI(view)
    }


    protected fun setLoading(show: Boolean?) {
        show?.let { mustShow ->
            context?.let {
                if (mustShow) {
                    loadingDialog?.dismiss()
                    loadingDialog = it.showLoadingDialog()
                } else {
                    loadingDialog?.dismiss()
                }
            }
        } ?: run {
            loadingDialog?.dismiss()
        }
    }
}