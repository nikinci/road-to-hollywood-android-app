package com.nikinci.roadtohollywood.extension

import android.content.Context
import okhttp3.ResponseBody
import java.io.IOException
import java.io.InputStream

fun ResponseBody.writeToDisk(appContext: Context, fileName: String): Boolean {
    return try {
        val wFile = java.io.File(appContext.filesDir, "$fileName.xlsx")
        val inputStream = this.byteStream()
        try {
            inputStream.use { input ->
                wFile.outputStream().use { output ->
                    input.copyTo(output)
                }
            }
            true
        } catch (e: IOException) {
            false
        } finally {
            // inputStream.close()
        }
    } catch (e: IOException) {
        false
    }
}

fun InputStream.writeToFile(appContext: Context, fileName: String): Boolean {
    val wFile = java.io.File(appContext.filesDir, "$fileName.xlsx")
    return try {
        this.use { input ->
            wFile.outputStream().use { output ->
                input.copyTo(output)
            }
        }
        true
    } catch (e: IOException) {
        false
    } finally {
        // inputStream.close()
    }

}

