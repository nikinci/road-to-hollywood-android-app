package com.nikinci.roadtohollywood.extension

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nikinci.roadtohollywood.R
import com.nikinci.roadtohollywood.util.*


/**
 * Created by nikinci on 11.16.2021.
 */
object RecyclerViewExtensions {
    enum class RecylerViewDecorationType {
        MARGIN, DIVIDER;

        companion object {
            fun getFromString(text: String): RecylerViewDecorationType =
                if (text.equals(DIVIDER.name)) {
                    MARGIN
                } else {
                    DIVIDER
                }
        }
    }

    enum class RecyclerViewLinearLayManagerType {
        VERTICAL, HORIZONTAL;
    }
}


fun <ITEM> RecyclerView.setUp(
    items: List<ITEM>,
    layoutResId: Int,
    bindHolder: View.(ITEM, Int) -> Unit,
    itemClick: ITEM.(Int) -> Unit = {},
    manager: RecyclerView.LayoutManager = LinearLayoutManager(this.context)
): GeneralAdapter<ITEM> {
    return GeneralAdapter(items, layoutResId, { item, position ->
        bindHolder(item, position)
    }, { pos ->
        itemClick(pos)
    }).apply {
        layoutManager = manager
        adapter = this
    }
}

@BindingAdapter(
    value = ["itemList", "itemLayoutResId", "bindholder", "click", "manager", "itemDecoration"],
    requireAll = false
)
fun <ITEM> RecyclerView.setUpBindingAdapter(
    items: Set<ITEM>?,
    layoutResId: Int,
    bindHolder: ((holder: AbstractRecyclerBindingAdapter.HolderBinding, ITEM, Int) -> Unit)? = null,
    itemClick: (ITEM.() -> Unit)? = null,
    manager: RecyclerView.LayoutManager?,
    itemDecoration: RecyclerView.ItemDecoration? = null
): GeneralBindingAdapter<ITEM>? {
    if (items.isNullOrEmpty()) {
        return null
    }
    /*   LinearLayoutManager(this.context).apply {
           orientation = RecyclerView.HORIZONTAL
       }*/

    return setUpBindingAdapter(
        items.toList(),
        layoutResId,
        bindHolder,
        itemClick,
        manager,
        itemDecoration
    )
}

@BindingAdapter(
    value = ["itemList", "itemLayoutResId", "bindholder", "click", "manager", "itemDecoration"],
    requireAll = false
)
fun <ITEM> RecyclerView.setUpBindingAdapter(
    items: List<ITEM>?,
    layoutResId: Int,
    bindHolder: ((holder: AbstractRecyclerBindingAdapter.HolderBinding, ITEM, Int) -> Unit)? = null,
    itemClick: (ITEM.() -> Unit)? = null,
    manager: RecyclerView.LayoutManager? = LinearLayoutManager(this.context).apply {
        orientation = RecyclerView.VERTICAL
    },
    itemDecoration: RecyclerView.ItemDecoration? = null
): GeneralBindingAdapter<ITEM>? {
    if (items.isNullOrEmpty()) {
        return null
    }
    return GeneralBindingAdapter(items, layoutResId, { holder, item, position ->
        bindHolder?.let {
            it(holder, item, position)
        }
    }, {
        itemClick?.let {
            it()
        }
    }).apply {
        manager?.let {
            layoutManager = it
        }
        itemDecoration?.let {
            addItemDecoration(it)
        }
        adapter = this
    }
}

@BindingAdapter(
    value = ["layManager", "itemDecorator"],
    requireAll = false
)
fun RecyclerView.layManager(
    layoutManagerType: RecyclerViewExtensions.RecyclerViewLinearLayManagerType? = null,
    itemDecorationType: RecyclerViewExtensions.RecylerViewDecorationType? = null
) {
    // ikisi beraber cunku layout manager verilmeden item decorator verilirse horizontal listede orientation farkli oluyor xml formatta once item decorator oldugu icin
    // once layout manager sonra item decorator
    layoutManagerType?.let {
        when (it) {
            RecyclerViewExtensions.RecyclerViewLinearLayManagerType.VERTICAL -> {
                layoutManager = LinearLayoutManager(this.context)
            }
            RecyclerViewExtensions.RecyclerViewLinearLayManagerType.HORIZONTAL -> {
                layoutManager = LinearLayoutManager(this.context).apply {
                    orientation = RecyclerView.VERTICAL
                }
            }
        }
    }

    itemDecorationType?.let {
        setItemDecorator(it)
    }
}


fun RecyclerView.setItemDecorator(type: RecyclerViewExtensions.RecylerViewDecorationType) {
    when (type) {
        RecyclerViewExtensions.RecylerViewDecorationType.DIVIDER -> {
            var orientation = DealDividerItemDecoration.VERTICAL
            layoutManager?.let {
                if (it.canScrollHorizontally()) {
                    orientation = DealDividerItemDecoration.HORIZONTAL
                }
            }
            addItemDecoration(
                DealDividerItemDecoration(context, orientation)
            )
        }
        RecyclerViewExtensions.RecylerViewDecorationType.MARGIN -> {
            var orientation = DealDividerItemDecoration.VERTICAL
            layoutManager?.let {
                if (it.canScrollHorizontally()) {
                    orientation = DealDividerItemDecoration.HORIZONTAL
                }
            }
            addItemDecoration(
                DealMarginItemDecoration(
                    resources.getDimension(R.dimen.app_padding_small).toInt(), orientation
                )
            )
        }
    }
}


interface OnItemClickListener {
    fun onItemClicked(position: Int, view: View)
}


fun RecyclerView.onItemClicked(function: (position: Int) -> Unit) {
    addOnItemClickListener(object : OnItemClickListener {
        override fun onItemClicked(position: Int, view: View) {
            function(position)
        }
    })
}

fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
    this.addOnChildAttachStateChangeListener(object :
        RecyclerView.OnChildAttachStateChangeListener {
        override fun onChildViewDetachedFromWindow(view: View) {
            view.setOnClickListener(null)
        }

        override fun onChildViewAttachedToWindow(view: View) {
            view.setOnClickListener {
                val holder = getChildViewHolder(view)
                onClickListener.onItemClicked(holder.adapterPosition, view)
            }
        }
    })
}


interface OnLoadMoreListener {
    fun onLoadMore()
}

fun RecyclerView.setOnLoadMoreListener(
    onLoadMoreListener: OnLoadMoreListener,
    visibleThreshold: Int = 5
) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)

            if (layoutManager == null) {
                throw IllegalStateException("layoutManager is null")
            }

            if (layoutManager is LinearLayoutManager) {
                val totalItemCount = layoutManager!!.itemCount
                val lastVisibleItemPosition =
                    (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                if (totalItemCount <= (lastVisibleItemPosition + visibleThreshold)) {
                    onLoadMoreListener.onLoadMore()
                }
            } else {
                throw IllegalStateException("layoutManager not a  LinearLayoutManager")
            }
        }
    })
}




