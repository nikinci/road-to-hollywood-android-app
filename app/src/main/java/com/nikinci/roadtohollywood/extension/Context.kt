package com.nikinci.roadtohollywood.extension

import android.app.Dialog
import android.content.Context
import android.widget.Toast
import com.nikinci.roadtohollywood.customViews.RHLoadingDialog

fun Context.showLoadingDialog(): Dialog {
    return RHLoadingDialog(this).apply {
        show()
    }
}

fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}