package com.nikinci.roadtohollywood.extension

import android.webkit.URLUtil

fun String.isValidUrl() = URLUtil.isValidUrl(this)