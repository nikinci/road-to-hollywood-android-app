package com.nikinci.roadtohollywood.extension

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

/**
 * Created by nikinci on 2019-03-19.
 */
@BindingAdapter(
    value = ["imageUrl", "placeholder", "error"],
    requireAll = false
)
fun ImageView.bindimageUrl(
    imageUrl: String?,
    placeholder: Drawable? = null,
    error: Drawable? = null
) {
    try {
        Glide
            .with(context)
            .load(imageUrl)
            .placeholder(placeholder)
            .error(error)
            .into(this)
    } catch (e: Exception) {
        e.message
    }
}




