package com.nikinci.roadtohollywood.extension

import java.text.SimpleDateFormat
import java.util.*

const val DATE_FORMAT = "dd/MM/yyyy HH:mm:ss"
const val DATE_FORMAT_EXCEL_TITLE = "MM.dd"

fun Date.formatRHDate(): String {
    return SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(this)
}

fun Date.formatExcelTitle(): String {
    return SimpleDateFormat(DATE_FORMAT_EXCEL_TITLE, Locale.getDefault()).format(this)
}