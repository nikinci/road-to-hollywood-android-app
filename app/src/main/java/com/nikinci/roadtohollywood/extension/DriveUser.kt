package com.nikinci.roadtohollywood.extension

import com.google.api.services.drive.model.User
import com.nikinci.roadtohollywood.model.RHUser

fun User.toRHUser(): RHUser {

    return RHUser(
        displayName = this.displayName,
        emailAddress = this.emailAddress,
        photoLink = this.photoLink
    )
}