package com.nikinci.roadtohollywood.extension

import com.google.api.services.drive.model.Revision
import com.nikinci.roadtohollywood.model.RHRevision
import java.util.*

fun Revision.toRHRevision(): RHRevision {
    val calendar = Calendar.getInstance()
    calendar.time = Date(this.modifiedTime.value)
    return RHRevision(
        id = this.id,
        user = this.lastModifyingUser.toRHUser(),
        modifieedTime = calendar.time,
        published = this.published

    )
}