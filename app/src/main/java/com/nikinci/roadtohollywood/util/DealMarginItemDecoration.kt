package com.nikinci.roadtohollywood.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class DealMarginItemDecoration(
    private val spaceHeight: Int,
    private val mOrientation: Int = DealDividerItemDecoration.VERTICAL
) : RecyclerView.ItemDecoration() {


    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {
        with(outRect) {
            when (mOrientation) {
                DealDividerItemDecoration.VERTICAL -> {
                    if (parent.getChildAdapterPosition(view) == 0) {
                        top = spaceHeight
                    }
                    bottom = spaceHeight
                }
                DealDividerItemDecoration.HORIZONTAL -> {
                    if (parent.getChildAdapterPosition(view) > 0) {
                        left = spaceHeight
                    }
                    right = spaceHeight
                }
            }


        }
    }
}