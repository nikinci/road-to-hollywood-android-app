package com.nikinci.roadtohollywood.util

import android.content.Context
import android.util.Log
import com.nikinci.roadtohollywood.model.RevisionDiff
import com.nikinci.roadtohollywood.extension.formatExcelTitle
import com.nikinci.roadtohollywood.model.RHRevision
import org.apache.poi.hssf.usermodel.HSSFDateUtil
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

object ExcelDiffer {

    val prevSheetMap = HashMap<Pair<Int, Int>, Cell>()
    val titleArray = arrayListOf<String>()
    val nameArray = arrayListOf<String>()

    fun diff2ExcelFile(
        context: Context,
        rev: RHRevision,
        prevRev: RHRevision
    ): MutableList<RevisionDiff> {
        prevSheetMap.clear()
        val diffList = mutableListOf<RevisionDiff>()

        val currBook = XSSFWorkbook(java.io.File(context.filesDir, rev.getFileName() + ".xlsx"))
        val sheet: XSSFSheet = currBook.getSheetAt(0)
        val rowIter: Iterator<Row> = sheet.rowIterator()

        val prevBook = XSSFWorkbook(java.io.File(context.filesDir, prevRev.getFileName() + ".xlsx"))
        val prevSheet: XSSFSheet = prevBook.getSheetAt(0)

        var rowNo = 0
        var userNameFinished = false
        while (rowIter.hasNext() && userNameFinished.not()) {
            val myRow = rowIter.next() as XSSFRow
            val cellIter: Iterator<Cell> = myRow.cellIterator()
            var userName = ""
            val userLogList = mutableListOf<Double>()
            if (rowNo == 4) {
                while (cellIter.hasNext() && userNameFinished.not()) {
                    val currentCell = cellIter.next()
                    titleArray.add(getCellValueAsString(currentCell))
                }
            } else if (rowNo >= 5) {
                var colno = 0
                while (cellIter.hasNext() && userNameFinished.not() && colno < 67) {
                    val currentCell = cellIter.next()
                    if (colno == 0) {
                        userName = currentCell.toString()
                        if (userName.isEmpty()) {
                            userNameFinished = true
                        } else {
                            nameArray.add(userName)
                        }
                    } else {


                        val prevCell = getCellAtSheet(prevSheet, rowNo, colno)
                        var hasDiff = false
                        if (currentCell.cellType != prevCell?.cellType) {
                            hasDiff = true

                        } else {

                            if (currentCell.cellType == 0) {
                                if (currentCell.numericCellValue != prevCell.numericCellValue) {
                                    hasDiff = true
                                }
                            } else if (currentCell.cellType == 1) {
                                if (currentCell.stringCellValue.equals(prevCell.stringCellValue)
                                        .not()
                                ) {
                                    hasDiff = true
                                }
                            } else if (currentCell.cellType == 2) {
                                if (currentCell.cellFormula != prevCell.cellFormula) {
                                    hasDiff = true
                                }
                            }

                        }

                        if (hasDiff) {
                            Log.d(
                                "Diff",
                                "rowNo:${rowNo + 1} rowName: ${nameArray[rowNo - 5]}, colno:$colno colName: ${titleArray[colno]} currentCell = ${
                                    getCellValueAsString(
                                        currentCell
                                    )
                                } , prevCell = ${
                                    getCellValueAsString(
                                        prevCell
                                    )
                                }"
                            )
                            diffList.add(RevisionDiff().apply {
                                this.rowNo = rowNo
                                this.rowName = nameArray[rowNo - 5]
                                this.colNo = colno
                                this.colName = titleArray[colno]
                                this.currentValue = getCellValueAsString(
                                    currentCell
                                )
                                this.oldValue = getCellValueAsString(
                                    prevCell
                                )
                            })
                        }


                        if (currentCell.toString().isNotEmpty()) {
                            userLogList.add(currentCell.numericCellValue)
                        }

                    }
                    colno++

                }

            }
            rowNo++
        }



        return diffList
    }


    fun getCellValueAsString(cell: Cell?): String {

        try {
            if (HSSFDateUtil.isCellDateFormatted(cell)) {

                cell?.dateCellValue?.let {
                    return it.formatExcelTitle()
                }
            }
        } catch (e: Exception) {

        }

        return when (cell?.cellType) {
            0 -> {
                cell.numericCellValue.toString()
            }
            1 -> {
                cell.stringCellValue
            }
            2 -> {
                cell.cellFormula.toString()
            }
            3 -> {
                ""
            }

            else -> ""
        }
    }


    fun getCellAtSheet(sheet: XSSFSheet, row: Int, col: Int): Cell? {
        if (prevSheetMap.isEmpty()) {
            fillPrevCellMap(sheet)
        }
        return prevSheetMap[Pair(row, col)]


    }

    fun fillPrevCellMap(sheet: XSSFSheet) {
        val rowIter: Iterator<Row> = sheet.rowIterator()
        var rowNo = 0
        var userNameFinished = false
        while (rowIter.hasNext() && userNameFinished.not()) {
            val currRow = rowIter.next() as XSSFRow
            val cellIter: Iterator<Cell> = currRow.cellIterator()
            if (rowNo >= 5) {
                var colno = 0
                while (cellIter.hasNext() && userNameFinished.not()) {
                    val currentCell = cellIter.next()
                    if (colno == 0) {
                        val userName = currentCell.toString()
                        if (userName.isEmpty()) {
                            userNameFinished = true
                        }
                    }

                    prevSheetMap[Pair(rowNo, colno)] = currentCell

                    colno++
                }
            }

            rowNo++
        }

    }
}