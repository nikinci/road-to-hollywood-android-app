package com.nikinci.roadtohollywood.data.source

import com.nikinci.roadtohollywood.model.GoogleDriveResponseDto
import com.nikinci.roadtohollywood.network.ResponseState
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response

/**
 * Created by nikinci on 11/16/21.
 */
interface GoogleDriveDataSource {
    suspend fun getDriveDocument(): ResponseState<GoogleDriveResponseDto>
    fun downLoadFile(url: String): Observable<Response<ResponseBody>>
}