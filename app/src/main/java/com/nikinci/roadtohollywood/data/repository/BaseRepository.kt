package com.nikinci.roadtohollywood.data.repository

import com.nikinci.roadtohollywood.network.ResponseState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

/**
 * Created by nikinci on 11/16/21.
 */
open class BaseRepository {
    fun <T : Any> apiCall(call: suspend () -> ResponseState<T>): Flow<ResponseState<T>> = flow {
        emit(ResponseState.Loading(true))
        emit(call.invoke())
        emit(ResponseState.Loading(false))
    }.catch { error ->
        error.message?.let { emit(ResponseState.Error(errorMessage = it)) }
    }

}