package com.nikinci.roadtohollywood.data.mock

import com.nikinci.roadtohollywood.data.source.GoogleDriveDataSource
import com.nikinci.roadtohollywood.model.GoogleDriveResponseDto
import com.nikinci.roadtohollywood.network.ResponseState
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by nikinci on 11/16/21.
 */
class GoogleDriveMockDataSource @Inject constructor() :
    GoogleDriveDataSource {

    //returns mock data
    override suspend fun getDriveDocument(): ResponseState<GoogleDriveResponseDto> {
        return ResponseState.Success(GoogleDriveResponseDto("testDoc"))
    }

    override fun downLoadFile(url: String): Observable<Response<ResponseBody>> {
        TODO("Not yet implemented")
    }


}



