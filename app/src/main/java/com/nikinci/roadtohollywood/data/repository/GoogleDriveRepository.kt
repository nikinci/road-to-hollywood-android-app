package com.nikinci.roadtohollywood.data.repository

import android.annotation.SuppressLint
import android.content.Context
import com.nikinci.roadtohollywood.data.source.GoogleDriveDataSource
import com.nikinci.roadtohollywood.model.GoogleDriveResponseDto
import com.nikinci.roadtohollywood.network.ResponseState
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

/**
 * Created by nikinci on 11/16/21.
 */
class GoogleDriveRepository @Inject constructor(
    private val dataSource: GoogleDriveDataSource,
    @ApplicationContext private val appContext: Context, okHttpClient: OkHttpClient
) :
    BaseRepository() {

    suspend fun getDriveDocument(): Flow<ResponseState<GoogleDriveResponseDto>> {

        return apiCall { dataSource.getDriveDocument() }
    }


    @SuppressLint("CheckResult")
    fun downloadAndSaveFile(url: String, fileName: String) {
        dataSource.downLoadFile(url).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                if (it.isSuccessful) {
                    writeResponseBodyToDisk(it.body()!!, fileName)
                }

            }, {

            })
    }


    @SuppressLint("CheckResult")
    fun downloadSingleFile(
        fileUrl: String, fileName: String,
        completed: (() -> Unit)?,
        error: (() -> Unit)?
    ) {

        dataSource.downLoadFile(fileUrl).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                { list ->
                    //list.forEachIndexed { index, any ->
                    val response = (list as Response<ResponseBody>)
                    response.raw().request.url
                    if (response.isSuccessful) {
                        writeResponseBodyToDisk(
                            response.body()!!,
                            fileName
                        )
                    }
                    // }
                    completed?.let { it() }
                },
                { e ->
                    error?.let { it() }
                    completed?.let { it() }
                }
            )

    }

    @SuppressLint("CheckResult")
    fun downloadFileList(
        pairList: List<Pair<String, String>>,
        completed: (() -> Unit)?,
        error: (() -> Unit)?

    ) {

        if (pairList.isEmpty()) {
            completed?.let { it() }
            return
        }
        val observableList: MutableList<Observable<Response<ResponseBody>>> =
            mutableListOf()
        pairList.forEach { pair ->
            val imgObservable = dataSource.downLoadFile(pair.first)
            imgObservable.subscribeOn(Schedulers.io())
            observableList.add(imgObservable)
        }

        var order = 0
        var successCount = 0
        Observable.concat(observableList).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                { list ->
                    //list.forEachIndexed { index, any ->
                    val response = (list as Response<ResponseBody>)
                    response.raw().request.url
                    if (response.isSuccessful) {
                        writeResponseBodyToDisk(
                            response.body()!!,
                            pairList[order].second
                        )
                        successCount++
                        //  }
                    }
                    order++

                },
                { e ->
                    error?.let { it() }
                }, {
                    if (order == successCount) {
                        completed?.let { it() }
                    } else {
                        error?.let { it() }
                    }
                }
            )


/*
        observableList.first()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                { list ->
                    //list.forEachIndexed { index, any ->
                    val response = (list as Response<ResponseBody>)
                    response.raw().request.url
                    if (response.isSuccessful) {
                        writeResponseBodyToDisk(
                            response.body()!!,
                            pairList[0].second
                        )
                    }
                    // }
                    completed?.let { it() }
                },
                { e ->
                    error?.let { it() }
                    completed?.let { it() }
                }
            )*/


/*
        Observable.zip(observableList) { args -> args }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                { list ->
                    list.forEachIndexed { index, any ->
                        val response = (any as Response<ResponseBody>)
                        response.raw().request.url
                        if (response.isSuccessful) {
                            writeResponseBodyToDisk(
                                response.body()!!,
                                pairList[index].second
                            )
                        }
                    }
                    completed?.let { it() }
                },
                { e ->
                    error?.let { it() }
                    completed?.let { it() }
                }
            )*/


    }

    private fun writeResponseBodyToDisk(body: ResponseBody, fileName: String): Boolean {
        return try {
            val wFile = java.io.File(appContext.filesDir, "$fileName.xlsx")
            val inputStream = body.byteStream()
            try {
                inputStream.use { input ->
                    wFile.outputStream().use { output ->
                        input.copyTo(output)
                    }
                }
                true
            } catch (e: IOException) {
                false
            } finally {
                // inputStream.close()
            }
        } catch (e: IOException) {
            false
        }
    }


}