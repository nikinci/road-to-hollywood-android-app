package com.nikinci.roadtohollywood.data.remote

import com.nikinci.roadtohollywood.data.source.GoogleDriveDataSource
import com.nikinci.roadtohollywood.model.GoogleDriveResponseDto
import com.nikinci.roadtohollywood.network.RHService
import com.nikinci.roadtohollywood.network.ResponseState
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by nikinci on 11/16/21.
 */
class GoogleDriveRemoteDataSource @Inject constructor(val service: RHService) :
    GoogleDriveDataSource {


    override suspend fun getDriveDocument(): ResponseState<GoogleDriveResponseDto> {
        return ResponseState.Success(GoogleDriveResponseDto("testDoc"))
    }

    override fun downLoadFile(url: String): Observable<Response<ResponseBody>> {
        return service.getFileObservable(url)
    }


}



