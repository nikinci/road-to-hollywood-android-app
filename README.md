## Road to Hollywood Android Application

This application get excel file from google drive and it lists revisions and differences in a revision. If you want to test it firstly request file access from nurullahikinci@gmail.com, then  [download apk ](https://disk.yandex.com.tr/d/GYFoyzneGH2U2Q)

[Review File](https://docs.google.com/spreadsheets/d/1VWuF4w6DZXcsmGrhKn9LVvygw2lNSX1JpVsfERntmWc/edit#gid=0)


## How to use application

When you open app you see revision list of that file. You can check differences every revision easily






### Home Page

Home page shows revision list.Every revision show modified user's name,email image and  modified date revision id.



![HomePreview](https://image.sicakfirsatlar.app/images/deal/1637161849920.webp)




### Revision Detail Page

If you click any revision you will be redirected to detail page. In that page application gets current and previous revision files then calculates differences, then show changes per row and column. You see differences in a row,column old value and new values.




![RevisionPreview](https://image.sicakfirsatlar.app/images/deal/1637161874360.webp)


### Contact me

If you'd like to contact me,  email nurullahikinci@gmail.com. Thank you!